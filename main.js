"use strict";
exports.__esModule = true;
var employees_1 = require("./employees");
var EmployeeProduction_1 = require("./EmployeeProduction");
var daytaler_1 = require("./daytaler");
var EmployeeManagement_1 = require("./EmployeeManagement");
var readlineSync = require('readline-sync');
function input(Employees) {
    var quantity = parseInt(readlineSync.question('Nhập số lượng nhân viên thêm vào: '));
    for (var i = 0; i < quantity; i++) {
        console.log("Nh\u1EADp nh\u00E2n vi\u00EAn th\u1EE9 " + (i + 1));
        Employees.getInput();
        Employees.addEmployees();
    }
}
function subitem() {
    console.log("---------------------------------------");
    console.log("|        1. Nhân viên sản xuất       |");
    console.log("|        2. Nhân viên công nhật      |");
    console.log("|        3. Nhân viên quản lí        |");
    console.log("---------------------------------------");
}
var main = /** @class */ (function () {
    function main() {
    }
    main.prototype.program = function () {
        console.log("------------------------------------------------------");
        console.log("|        1. Thêm nhân viên vào danh sách công ty    |");
        console.log("|        2. Xem danh sách Nhân viên                 |");
        console.log("|        3. Tính lương cho nhân viên                |");
        console.log("-----------------------------------------------------");
        var choose = parseInt(readlineSync.question('nhập lựa chọn của bạn '));
        var typeEmployees;
        var employee;
        switch (choose) {
            case 1:
                subitem();
                typeEmployees = parseInt(readlineSync.question('Nhập mã loại nhân viên: '));
                if (typeEmployees == 1) {
                    employee = new EmployeeProduction_1.EmployeeProduction();
                    input(employee);
                }
                if (typeEmployees == 2) {
                    employee = new daytaler_1.daytaler();
                    input(employee);
                }
                if (typeEmployees == 3) {
                    employee = new EmployeeManagement_1.EmployeeManagement();
                    input(employee);
                }
                this.program();
                break;
            case 2:
                subitem();
                typeEmployees = parseInt(readlineSync.question('Nhập mã loại nhân viên: '));
                if (typeEmployees == 1) {
                    employee = new EmployeeProduction_1.EmployeeProduction();
                    employee.getListEmployees();
                }
                if (typeEmployees == 2) {
                    employee = new daytaler_1.daytaler();
                    employee.getListEmployees();
                }
                if (typeEmployees == 3) {
                    employee = new EmployeeManagement_1.EmployeeManagement();
                    employee.getListEmployees();
                }
                this.program();
                break;
            case 3:
                console.log("-----------------------------------------------------------");
                console.log("|        1. Xem tổng lương các nhân viên trong công ty    |");
                console.log("|        2. Nhân viên có lương cao nhất                   |");
                console.log("|        3. Nhân viên có lương thấp nhất                  |");
                console.log("-----------------------------------------------------------");
                typeEmployees = parseInt(readlineSync.question('Nhập mã loại nhân viên: '));
                var list = [];
                list = list.concat(EmployeeProduction_1.EmployeeProduction.list, EmployeeManagement_1.EmployeeManagement.list, daytaler_1.daytaler.list);
                if (typeEmployees == 1) {
                    var sum = 0;
                    for (var i = 0; i < list.length; i++) {
                        sum += list[i][3];
                    }
                    console.log("T\u1ED5ng l\u01B0\u01A1ng c\u1EE7a c\u00F4ng ty: " + sum);
                }
                if (typeEmployees == 2) {
                    var max_1 = Math.max.apply(Math, Array.from(employees_1.Employees.listEmployees, function (salary) { return salary[3]; }));
                    var emp = employees_1.Employees.listEmployees.filter(function (arr) {
                        return arr[3] === max_1;
                    });
                    console.log("Nhân viên có lương cao nhất");
                    console.table(['Họ tên', 'Ngày sinh', "Địa chỉ", "Lương"], emp);
                }
                if (typeEmployees == 3) {
                    var min_1 = Math.min.apply(Math, Array.from(employees_1.Employees.listEmployees, function (salary) { return salary[3]; }));
                    var emp = employees_1.Employees.listEmployees.filter(function (arr) {
                        return arr[3] === min_1;
                    });
                    console.log("Nhân viên có lương thấp nhất");
                    console.table(['Họ tên', 'Ngày sinh', "Địa chỉ", "Lương"], emp);
                }
                this.program();
                break;
            default:
        }
    };
    return main;
}());
var m = new main();
m.program();
