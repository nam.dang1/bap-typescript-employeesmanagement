import {Employees} from "./employees";
let readlineSync = require('readline-sync');
const cTable = require('console.table');
class daytaler extends Employees {
    static list: any[] = [];
    private date: number;
    public constructor(fullName?: string, birthday?: Date, address?: string, date?: number) {
        super(fullName, birthday, address);
        this.date = !date ? 0 : date;
    }

    public getInput(): void {
        super.getInput();
        this.date =  parseInt(readlineSync.question('    Nhập số  lượng Ngày công? '));
    }
    public getListEmployees(): void {
        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ","Lương"], daytaler.list);
    }
    public Salary(): number {
        return this.date * 50000;
    }
    public addEmployees(): void {
        let arr: [string, Date, string, any] = [this.fullName, this.birthday, this.address, this.Salary()];
        daytaler.list.push(arr);
        Employees.listEmployees = Employees.listEmployees.concat(daytaler.list);
    }
}
export {daytaler};