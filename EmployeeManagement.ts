import {Employees} from "./employees";
let readlineSync = require('readline-sync');
const cTable = require('console.table');
class EmployeeManagement extends Employees {
    static list: any[] = [];
    
    private basicSalary: number;
    private coefficient: number;

    public constructor(fullName?: string, birthday?: Date, address?: string, basicSalary?: number, coefficient?: number) {
        super(fullName, birthday, address);
        this.basicSalary = !basicSalary ? 0 : basicSalary;
        this.coefficient = !coefficient ? 0 : coefficient;
    }

    public getInput(): void {
        super.getInput();
        this.basicSalary =  parseInt(readlineSync.question('    Nhập Lương cơ bản '));
        this.coefficient =  parseInt(readlineSync.question('    Nhập Hệ số lương '));

    }
    public getListEmployees(): void {
        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ","Lương"], EmployeeManagement.list);
    }
    public Salary(): number {
        return this.basicSalary * this.coefficient;
    }

    public addEmployees(): void {
        let arr: [string, Date, string, any] = [this.fullName, this.birthday, this.address, this.Salary()];
        EmployeeManagement.list.push(arr);
        Employees.listEmployees = Employees.listEmployees.concat(EmployeeManagement.list);
    }

}
export {EmployeeManagement};