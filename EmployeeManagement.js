"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var employees_1 = require("./employees");
var readlineSync = require('readline-sync');
var cTable = require('console.table');
var EmployeeManagement = /** @class */ (function (_super) {
    __extends(EmployeeManagement, _super);
    function EmployeeManagement(fullName, birthday, address, basicSalary, coefficient) {
        var _this = _super.call(this, fullName, birthday, address) || this;
        _this.basicSalary = !basicSalary ? 0 : basicSalary;
        _this.coefficient = !coefficient ? 0 : coefficient;
        return _this;
    }
    EmployeeManagement.prototype.getInput = function () {
        _super.prototype.getInput.call(this);
        this.basicSalary = parseInt(readlineSync.question('    Nhập Lương cơ bản '));
        this.coefficient = parseInt(readlineSync.question('    Nhập Hệ số lương '));
    };
    EmployeeManagement.prototype.getListEmployees = function () {
        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ", "Lương"], EmployeeManagement.list);
    };
    EmployeeManagement.prototype.Salary = function () {
        return this.basicSalary * this.coefficient;
    };
    EmployeeManagement.prototype.addEmployees = function () {
        var arr = [this.fullName, this.birthday, this.address, this.Salary()];
        EmployeeManagement.list.push(arr);
        employees_1.Employees.listEmployees = employees_1.Employees.listEmployees.concat(EmployeeManagement.list);
    };
    EmployeeManagement.list = [];
    return EmployeeManagement;
}(employees_1.Employees));
exports.EmployeeManagement = EmployeeManagement;
