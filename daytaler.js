"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var employees_1 = require("./employees");
var readlineSync = require('readline-sync');
var cTable = require('console.table');
var daytaler = /** @class */ (function (_super) {
    __extends(daytaler, _super);
    function daytaler(fullName, birthday, address, date) {
        var _this = _super.call(this, fullName, birthday, address) || this;
        _this.date = !date ? 0 : date;
        return _this;
    }
    daytaler.prototype.getInput = function () {
        _super.prototype.getInput.call(this);
        this.date = parseInt(readlineSync.question('    Nhập số  lượng Ngày công? '));
    };
    daytaler.prototype.getListEmployees = function () {
        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ", "Lương"], daytaler.list);
    };
    daytaler.prototype.Salary = function () {
        return this.date * 50000;
    };
    daytaler.prototype.addEmployees = function () {
        var arr = [this.fullName, this.birthday, this.address, this.Salary()];
        daytaler.list.push(arr);
        employees_1.Employees.listEmployees = employees_1.Employees.listEmployees.concat(daytaler.list);
    };
    daytaler.list = [];
    return daytaler;
}(employees_1.Employees));
exports.daytaler = daytaler;
