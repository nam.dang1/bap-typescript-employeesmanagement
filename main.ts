import {Employees} from "./employees";
import {EmployeeProduction} from "./EmployeeProduction";
import {daytaler} from "./daytaler";
import {EmployeeManagement} from "./EmployeeManagement";

let readlineSync = require('readline-sync');

function input(Employees: any){
    let quantity = parseInt(readlineSync.question('Nhập số lượng nhân viên thêm vào: '));
    for(let i =0; i<quantity; i++){
        console.log(`Nhập nhân viên thứ ${i+1}`);
        Employees.getInput();
        Employees.addEmployees();
    }
   
}
function subitem(){
    console.log("---------------------------------------");
    console.log("|        1. Nhân viên sản xuất       |");
    console.log("|        2. Nhân viên công nhật      |");
    console.log("|        3. Nhân viên quản lí        |");
    console.log("---------------------------------------");
}
class main{
    public  program() {
        console.log("------------------------------------------------------");
        console.log("|        1. Thêm nhân viên vào danh sách công ty    |");
        console.log("|        2. Xem danh sách Nhân viên                 |");
        console.log("|        3. Tính lương cho nhân viên                |");
        console.log("-----------------------------------------------------");
        let choose: number = parseInt(readlineSync.question('nhập lựa chọn của bạn '));
        let typeEmployees:number;
        let employee:Employees;
        switch (choose) {
            case 1:
                subitem();
                 typeEmployees = parseInt(readlineSync.question('Nhập mã loại nhân viên: '));
               
                if(typeEmployees ==1){
                    employee = new EmployeeProduction();
                    input(employee);
                }
                if(typeEmployees ==2){
                    employee = new daytaler();
                    input(employee);
                }
                if(typeEmployees ==3){
                    employee = new EmployeeManagement();
                    input(employee);
                }
                this.program();
                break;
            case 2:
                    subitem();
                    typeEmployees = parseInt(readlineSync.question('Nhập mã loại nhân viên: '));
                    if(typeEmployees ==1){
                        employee = new EmployeeProduction();
                        employee.getListEmployees();
                    }
                    if(typeEmployees ==2){
                        employee = new daytaler();
                        employee.getListEmployees();
                    }
                    if(typeEmployees ==3){
                        employee = new EmployeeManagement();
                        employee.getListEmployees();
                    }
                    this.program();
                break;
            case 3:
                    console.log("-----------------------------------------------------------");
                    console.log("|        1. Xem tổng lương các nhân viên trong công ty    |");
                    console.log("|        2. Nhân viên có lương cao nhất                   |");
                    console.log("|        3. Nhân viên có lương thấp nhất                  |");
                    console.log("-----------------------------------------------------------");
                    typeEmployees = parseInt(readlineSync.question('Nhập mã loại nhân viên: '));
                    let list: any = [];
                   
                    list = list.concat(EmployeeProduction.list,EmployeeManagement.list,daytaler.list);
                    if(typeEmployees ==1){
                       
                        let sum =0;
                        for(let i: number = 0; i< list.length; i++){
                            sum += list[i][3];
                            

                        }
                       console.log(`Tổng lương của công ty: ${sum}`);
                      
                    }
                    if(typeEmployees ==2){

                        let max =  Math.max(...Array.from(Employees.listEmployees, salary => salary[3]));
                        
                        let emp = Employees.listEmployees.filter(function(arr) {
                            return arr[3] === max;
                        });
                        console.log("Nhân viên có lương cao nhất");
    
                        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ","Lương"],emp);
                    }
                    if(typeEmployees ==3){
                       
                        let min =  Math.min(...Array.from(Employees.listEmployees, salary => salary[3]));
                        
                       let emp = Employees.listEmployees.filter(function(arr) {
                            return arr[3] === min;
                        });
                        console.log("Nhân viên có lương thấp nhất");
    
                        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ","Lương"],emp);
                    }
                    this.program();
                    break;
            default:
           
        }
    }
}

let m = new main();
m.program();
