"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var employees_1 = require("./employees");
var readlineSync = require('readline-sync');
var cTable = require('console.table');
var EmployeeProduction = /** @class */ (function (_super) {
    __extends(EmployeeProduction, _super);
    function EmployeeProduction(fullName, birthday, address, quantityProduct) {
        var _this = _super.call(this, fullName, birthday, address) || this;
        _this.quantityProduct = !quantityProduct ? 0 : quantityProduct;
        return _this;
    }
    EmployeeProduction.prototype.getInput = function () {
        _super.prototype.getInput.call(this);
        this.quantityProduct = parseInt(readlineSync.question('    Nhập số  lượng sản phẩm? '));
    };
    EmployeeProduction.prototype.getListEmployees = function () {
        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ", "Lương"], EmployeeProduction.list);
    };
    EmployeeProduction.prototype.Salary = function () {
        return this.quantityProduct * 20000;
    };
    EmployeeProduction.prototype.addEmployees = function () {
        var arr = [this.fullName, this.birthday, this.address, this.Salary()];
        EmployeeProduction.list.push(arr);
        employees_1.Employees.listEmployees = employees_1.Employees.listEmployees.concat(EmployeeProduction.list);
    };
    EmployeeProduction.list = [];
    return EmployeeProduction;
}(employees_1.Employees));
exports.EmployeeProduction = EmployeeProduction;
