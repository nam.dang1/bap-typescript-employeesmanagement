import {Employees} from "./employees";
let readlineSync = require('readline-sync');
const cTable = require('console.table');
class EmployeeProduction extends Employees {
    static list: any[] = [];
    
    private quantityProduct: number;

    public constructor(fullName?: string, birthday?: Date, address?: string, quantityProduct?: number) {
        super(fullName, birthday, address);
        this.quantityProduct = !quantityProduct ? 0 : quantityProduct;
    }

    public getInput(): void {
        super.getInput();
        this.quantityProduct =  parseInt(readlineSync.question('    Nhập số  lượng sản phẩm? '));

    }
    public getListEmployees(): void {
        console.table(['Họ tên', 'Ngày sinh', "Địa chỉ","Lương"], EmployeeProduction.list);
    }
    public Salary(): number {
        return this.quantityProduct * 20000;
    }

    public addEmployees(): void {
        let arr: [string, Date, string, any] = [this.fullName, this.birthday, this.address, this.Salary()];
        EmployeeProduction.list.push(arr);
        Employees.listEmployees = Employees.listEmployees.concat(EmployeeProduction.list);
       
    }
}
export {EmployeeProduction};