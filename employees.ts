let readlineSync = require('readline-sync');
const cTable = require('console.table');
abstract class Employees {
    protected fullName: string;
    protected birthday: Date;
    protected address: string;

    static listEmployees: any[] = [];
    public constructor(fullName?: string, birthday?: Date, address?: string) {
        this.fullName = !fullName ? "" : fullName;
        this.birthday = !birthday ? new Date(0) : birthday;
        this.address = !address ? "" : address;
    }

    public get getFullName(): string {
        return this.fullName;
    }
    public set setFullName(fullName: string) {
        this.fullName = fullName;
    }
    public get getBirthday(): Date {
        return this.birthday;
    }
    public set setBirthday(birthday: Date) {
        this.birthday = birthday;
    }
    public get getAddress(): string {
        return this.address;
    }
    public set setAddress(address: string) {
        this.address = address;
    }

    protected getInput(): void {
        this.fullName = readlineSync.question('    Nhập tên của bạn? ');
        this.birthday = readlineSync.question('    Nhập ngày sinh của bạn? ');
        this.address = readlineSync.question( '    Nhập địa chỉ của bạn? ');

    };
    abstract addEmployees(): void;
    abstract getListEmployees(): void;
    abstract Salary(): number;
}


export {Employees};






