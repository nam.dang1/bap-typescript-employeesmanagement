"use strict";
exports.__esModule = true;
var readlineSync = require('readline-sync');
var cTable = require('console.table');
var Employees = /** @class */ (function () {
    function Employees(fullName, birthday, address) {
        this.fullName = !fullName ? "" : fullName;
        this.birthday = !birthday ? new Date(0) : birthday;
        this.address = !address ? "" : address;
    }
    Object.defineProperty(Employees.prototype, "getFullName", {
        get: function () {
            return this.fullName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Employees.prototype, "setFullName", {
        set: function (fullName) {
            this.fullName = fullName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Employees.prototype, "getBirthday", {
        get: function () {
            return this.birthday;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Employees.prototype, "setBirthday", {
        set: function (birthday) {
            this.birthday = birthday;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Employees.prototype, "getAddress", {
        get: function () {
            return this.address;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Employees.prototype, "setAddress", {
        set: function (address) {
            this.address = address;
        },
        enumerable: true,
        configurable: true
    });
    Employees.prototype.getInput = function () {
        this.fullName = readlineSync.question('    Nhập tên của bạn? ');
        this.birthday = readlineSync.question('    Nhập ngày sinh của bạn? ');
        this.address = readlineSync.question('     Nhập địa chỉ của bạn? ');
    };
    ;
    Employees.listEmployees = [];
    return Employees;
}());
exports.Employees = Employees;
